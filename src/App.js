import React, { Component } from "react";
// import Image from "./Component/Image.jpg";
import axios from "axios";
import Weather from "./Component/Weather";
import Form from "./Component/Form";
import "./App.css";
class App extends Component {
  state = {
    // humidity: "",
    // temperature: "",
    // visibility: "",
    // description: "",
    // pressure: "",
    // location: "",
    displayData: false,
    displayError: false,
    res: {},
  };
  handleSubmit = async (item) => {
    // console.log("item", item);
    await axios
      .get(
        `http://api.openweathermap.org/data/2.5/weather?q=${item.city},${item.country}&appid=8d2de98e089f1c28e1a22fc19a24ef04`
      )
      .then((Response) => {
        let res = Response.data;
        // console.log("response", res);
        this.setState({
          res: {
            humidity: res.main.humidity,
            temperature: res.main.temp,
            visibility: res.visibility,
            // description: res.weather[0].description,
            location: res.name,
            pressure: res.main.pressure,
          },
          displayData: true,
          displayError: false,
        });
      })
      .catch((e) => this.setState({ displayError: true, displayData: false }));
  };

  render() {
    return (
      <div className='main_div'>
        <div className='container-fluid col-md-12'>
          <h3 className='header'> Weather-App </h3>
          <div className='row'>
            <div className='col-md-6'>
              <Form onSubmitProps={this.handleSubmit} />
            </div>

            {this.state.displayData && <Weather climate={this.state.res} />}
            {this.state.displayError && (
              <h4 className='error' style={{ color: "red" }}>
                City not found
              </h4>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default App;

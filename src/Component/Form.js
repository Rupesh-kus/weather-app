import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      country: "",
      city: "",
    };
  }

  handleChange = (e) => {
    console.log(e.target.value);
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit(e) {
    e.preventDefault();
    //console.log("submited");
    // this.props.onSubmitProps(this.state);
    const data = {
      country: this.state.country,
      city: this.state.city,
    };
    this.props.onSubmitProps(data);
    this.setState({ city: "", country: "" });
  }

  render() {
    //console.log(this.state);
    return (
      <div>
        <form onSubmit={this.onSubmit.bind(this)}>
          <div className='container-fluid col-md-6'>
            <div className='row'>
              <div className='input'>
                <div className='col'>
                  <label>Country : </label>
                  <input
                    type='text'
                    onChange={this.handleChange}
                    value={this.state.country}
                    name='country'
                    className='form-control'
                    placeholder='Country name'
                  />
                </div>

                <div className='col'>
                  <label>City : </label>
                  <input
                    type='text'
                    onChange={this.handleChange}
                    name='city'
                    value={this.state.city}
                    className='form-control'
                    placeholder='City name'
                  />
                </div>
              </div>
            </div>
            <br />
            <button type='submit' className='btn btn-success'>
              Get Weather
            </button>
          </div>
        </form>
      </div>
    );
  }
}
export default Form;

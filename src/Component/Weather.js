import React, { Component } from "react";

class Weather extends Component {
  render() {
    console.log(this.props.climate, "data");
    return (
      <div className='weather'>
        <h2> Present Weather</h2>
        <div>
          <div class='card-deck'>
            <div class='card'>
              <div class='card-body'>
                <h6 class='card-title'>
                  temperature={this.props.climate.temperature}
                </h6>
                <i class='fas fa-thermometer-quarter'></i>
              </div>
            </div>
            <div class='card'>
              <div class='card-body'>
                <h6 class='card-title'>
                  humidity={this.props.climate.humidity}
                </h6>
                <i class='fas fa-wind'></i>
              </div>
            </div>
            <div class='card'>
              <div class='card-body'>
                <h6 class='card-title'>
                  visibility={this.props.climate.visibility}
                </h6>
                <i class='fal fa-fog'></i>
              </div>
            </div>
            <div class='card'>
              <div class='card-body'>
                <h6 class='card-title'>
                  pressure={this.props.climate.pressure}
                </h6>
              </div>
            </div>
            <div class='card'>
              <div class='card-body'>
                <h6 class='card-title'>
                  <li>location</li>
                  <li>{this.props.climate.location}</li>
                </h6>
                <i class='fas fa-map-marker-alt'></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Weather;

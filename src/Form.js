import React, { Component } from "react";
import Thunder from "./Thunder.jpg";

class Form extends Component {
  state = {
    country: "",
    city: "",
  };

  handleChange = (e) => {
    console.log(e.target.value);
    this.setState({
      name: e.target.value,
    });
  };

  handleSubmit = () => {
    let data = {
      city: this.state.city,
      country: this.state.country,
    };
  };
  render() {
    return (
      <div>
        <form>
          <div class=' text-center'>
            <h2> Current Weather</h2>
          </div>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-md-6'>
                <img
                  className='image-fluid'
                  src={Thunder}
                  alt='img not found'
                />
              </div>
              <div className='col-md-6'>
                <label> Country : </label>
                <input
                  placeholder='input country name'
                  onChange={this.handleChange}
                />
                <br />
                <label> City : </label>
                <input
                  placeholder='input city name'
                  onChange={this.handleChange}
                />
                <br />
                <button onSubmit={this.handleSubmit}>Search</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
export default Form;
